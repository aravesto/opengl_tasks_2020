//
// Created by andrew on 03-Mar-21.
//

#include <vector>
#include <cmath>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include "common/Application.hpp"
#include "common/Application.cpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"


/*
                3
   (0, 0) * -------- * (1, 0)
          |  \       |
          |   \   a  |
         6|   4\1    |2
          |     \    |
          |  b   \   |
   (0, 1) * -------- * (1, 1)
               5
 */

static const int neighbours[6][2] = {
        {0, 0}, // line 1
        {1, 1}, // line 2 } - a
        {1, 0}, // line 3

        {0, 0}, // line 4
        {1, 1}, // line 5 } - b
        {0, 1}, // line 6
};

const int size = 1000; // квадратная карта

float interpolate(float a0, float a1, float w) {
    return (a1 - a0) * ((w * (w * 6.0f - 15.0f) + 10.0f) * w * w * w) + a0; // smootherstep
}

glm::vec2 randomGradient(int x, int y) {
    float random = 2920.f * sinf(float(x) * 21942.f + float(y) * 171324.f + 8912.f) * cosf(float(x) * 23157.f * float(y) * 217832.f + 9758.f);
    return glm::vec2(cosf(random), sinf(random)) / 2.f;
}

float iNoise(glm::vec2 v) {
    const float smoothness = 5; // чем больше тем более гладко, но менее интересно

    int x0 = int(v.x / smoothness) * int(smoothness);
    int y0 = int(v.y / smoothness) * int(smoothness);
    int x1 = x0 + int(smoothness) ;
    int y1 = y0 + int(smoothness);

    float dx = (v.x - float(x0)) / smoothness;
    float dy = (v.y - float(y0)) / smoothness;
    float dx1 = (v.x - float(x1)) / smoothness;
    float dy1 = (v.y - float(y1)) / smoothness;

    auto p1 = glm::dot(glm::vec2(dx, dy), randomGradient(x0, y0));
    auto p2 = glm::dot(glm::vec2(dx1, dy)  , randomGradient(x1, y0));
    auto i1 = interpolate(p1, p2, dx);

    p1 = glm::dot(glm::vec2(dx, dy1), randomGradient(x0, y1));
    p2 = glm::dot(glm::vec2(dx1, dy1), randomGradient(x1, y1));
    auto i2 = interpolate(p1, p2, dx);
    return interpolate(i1, i2, dy);
}

float NoiseAt(glm::vec2 v) {
    float value = 0;
    for (int o = 0; o < 4; o++) {
        float fq = powf(2, float(o));
        float amp = powf(0.5, float(o));
        float zoom = 0.1;
        value += iNoise(v * fq * zoom) * amp;
    }
    return value;
}

// Вычисление нормалей есть в задании, но оно никак не требуется пока что
glm::vec3 NormalAt(glm::vec2 v) {
    auto left = NoiseAt(glm::clamp(v - glm::vec2(1, 0), 0.0f, 1.0f));
    auto right = NoiseAt(glm::clamp(v + glm::vec2(1, 0), 0.0f, 1.0f));
    auto up = NoiseAt(glm::clamp(v + glm::vec2(0, 1), 0.0f, 1.0f));
    auto down = NoiseAt(glm::clamp(v - glm::vec2(0, 1), 0.0f, 1.0f));
    return glm::normalize(glm::cross(glm::vec3(2 * 1, right - left, 0),
                                        glm::vec3(0, up - down, 2 * 1)));
}

class Relief : public Application {
private:
    void draw() override {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
        _shader->use();
        _surface->draw();

        Application::draw();
    }

    void makeScene() override {
        Application::makeScene();

        std::vector<glm::vec3> points;
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                for (auto & neighbour : neighbours) {
                    auto nxt = glm::vec2(i + neighbour[0], j + neighbour[1]);
                    points.emplace_back(glm::vec3(nxt / float(size), NoiseAt(nxt)));
                }
            }
        }

        DataBufferPtr buf_points = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf_points->setData(points.size() * sizeof(float) * 3, points.data());
        _surface = std::make_shared<Mesh>();
        _surface->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf_points);
        _surface->setPrimitiveType(GL_TRIANGLES);
        _surface->setVertexCount(points.size());
        // переместить игрока в центр и увеличить карту
        glm::tmat4x4<double> m = glm::translate(glm::mat4(1.0), glm::vec3(0, -10, 2.3)) * glm::scale(glm::vec3(20, 20, 1));
        _surface->setModelMatrix(m);

        _cameraMover = std::make_shared<FreeCameraMover>();
        _shader = std::make_shared<ShaderProgram>("795RomashchenkoData1/shader.vert", "795RomashchenkoData1/shader.frag");
    }

    MeshPtr _surface;
    ShaderProgramPtr _shader;
};

int main() {
    Relief r;
    r.start();
    return 0;
}