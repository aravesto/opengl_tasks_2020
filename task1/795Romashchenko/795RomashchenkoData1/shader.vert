#version 330

uniform mat4 modelMatrix; // нельзя предпосчитать на CPU - потому что повлияет на высоту
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;

out vec4 color;

void main()
{
    float h = vertexPosition.z;
    float r = (h + 0.5) * int(h > 0.05);
    float g = (h + 0.5) * int(h > 0.05);
    float b = int(h < 0.05)  + int(h > 0.15) * (h + 0.5);

    color.rgb = vec3(r, g, b);
    color.a = 1;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}